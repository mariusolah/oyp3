<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Produse;
use App\Entity\Contact;

class HomePageController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function new(Request $request)
    {
        $produse = $this->getDoctrine()->getRepository(Produse::class)->findAll();
        $contact = new Contact();

        $form = $this->createFormBuilder($contact)
         ->add('email', TextType::class, [
             'label' => ' ',
             'attr' => [
                 'value' => ''
             ]
         ])
         ->add('save', SubmitType::class, [
             'label' => 'Te vom contacta!',
             'attr' => ['class' => 'btn bg-danger text-white font-weight-bold mt-2'],
             ])
         ->getForm();

         $form->handleRequest($request);

         if($form->isSubmitted() && $form->isValid()){
             $contact = $form->getData();
             $enitityManager = $this->getDoctrine()->getManager();
             $enitityManager->persist($contact);
             $enitityManager->flush();
         } 

        return $this->render('home_page/index.html.twig', [
            'form' => $form->createView(),
            'produse' => $produse,
        ]);
    }
}
