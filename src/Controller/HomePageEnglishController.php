<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpClient\HttpClient;
use App\Entity\Contact;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class HomePageEnglishController extends AbstractController
{
    /**
     * @Route("/home/english", name="home_page_english")
     */
    public function new(Request $request)
    {
        $client = HttpClient::create();
        $response = $client->request('GET','http://127.0.0.1:8001/api/products.json');
        $contact = new Contact();

        $form = $this->createFormBuilder($contact)
        ->add('email', TextType::class, [
            'label' => ' ',
            'attr' => [
                'value' => ''
            ]
        ])
        ->add('save', SubmitType::class, [
            'label' => 'We will contact you!',
            'attr' => ['class' => 'btn bg-danger text-white font-weight-bold mt-2'],
            ])
        ->getForm();

        $form->handleRequest($request);
        $content = $response->toArray();

        if($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($contact);
            $enitityManager->flush();
        } 

        return $this->render('home_page_english/index.html.twig', [
            'content' =>$content,
            'form' => $form->createView(),
        ]);
    }
}
