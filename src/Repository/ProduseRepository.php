<?php

namespace App\Repository;

use App\Entity\Produse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Produse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produse[]    findAll()
 * @method Produse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produse::class);
    }

    // /**
    //  * @return Produse[] Returns an array of Produse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produse
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
